package com.proiect.business.controller;

import com.proiect.business.model.Book;
import jakarta.servlet.http.HttpServletResponse;
import lombok.AllArgsConstructor;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.*;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

import java.util.List;


@RestController
@AllArgsConstructor
@RequiredArgsConstructor
@RequestMapping("/books")
public class BookController {
    @Value("${docker.ioService}")
    private String ioServiceURL;

    @Value("${docker.authService}")
    private String authServiceURL;

    @GetMapping
    public List<Book> getBooks(@RequestHeader("Authorization") String authorizationHeader, HttpServletResponse r) {

        RestTemplate restTemplate1 = new RestTemplate();
        HttpHeaders headers = new HttpHeaders();
        headers.set("Authorization", authorizationHeader);
        HttpEntity<Void> requestEntity = new HttpEntity<>(headers);

        try {
            ResponseEntity<Void> response = restTemplate1.exchange(authServiceURL + "/api/v1/validate", HttpMethod.GET, requestEntity, Void.class);
        } catch (HttpClientErrorException e) {
            r.setStatus(HttpStatus.UNAUTHORIZED.value());
            return null;
        }

        RestTemplate restTemplate = new RestTemplate();
        ParameterizedTypeReference<List<Book>> responseType =
                new ParameterizedTypeReference<>() {
                };

        return restTemplate.exchange(ioServiceURL + "/api/books", HttpMethod.GET, null, responseType).getBody();
    }

    @PostMapping
    public Book addBook(@RequestBody Book book, @RequestHeader("Authorization") String authorizationHeader, HttpServletResponse r) {

        RestTemplate restTemplate1 = new RestTemplate();
        HttpHeaders headers1 = new HttpHeaders();
        headers1.set("Authorization", authorizationHeader);
        HttpEntity<Void> requestEntity1 = new HttpEntity<>(headers1);

        try {
            ResponseEntity<Void> response = restTemplate1.exchange(authServiceURL + "/api/v1/validate", HttpMethod.GET, requestEntity1, Void.class);
        } catch (HttpClientErrorException e) {
            r.setStatus(HttpStatus.UNAUTHORIZED.value());
            return null;
        }

        RestTemplate restTemplate = new RestTemplate();
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);

        HttpEntity<Book> requestEntity = new HttpEntity<>(book, headers);

        return restTemplate.postForEntity(ioServiceURL + "/api/books", requestEntity, Book.class).getBody();
    }

    @PutMapping("{bookId}")
    public Book updateBook(@PathVariable Integer bookId, @RequestBody Book book, @RequestHeader("Authorization") String authorizationHeader, HttpServletResponse r) {

        RestTemplate restTemplate1 = new RestTemplate();
        HttpHeaders headers1 = new HttpHeaders();
        headers1.set("Authorization", authorizationHeader);
        HttpEntity<Void> requestEntity1 = new HttpEntity<>(headers1);

        try {
            ResponseEntity<Void> response = restTemplate1.exchange(authServiceURL + "/api/v1/validate", HttpMethod.GET, requestEntity1, Void.class);
        } catch (HttpClientErrorException e) {
            r.setStatus(HttpStatus.UNAUTHORIZED.value());
            return null;
        }

        RestTemplate restTemplate = new RestTemplate();
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);

        HttpEntity<Book> requestEntity = new HttpEntity<>(book, headers);
        String updateBookUrl = ioServiceURL + "/api/books" + "/" + bookId;
        return restTemplate.exchange(updateBookUrl, HttpMethod.PUT, requestEntity, Book.class, bookId).getBody();
    }

    @DeleteMapping("{bookId}")
    public String deleteBook(@PathVariable Integer bookId, @RequestHeader("Authorization") String authorizationHeader, HttpServletResponse r) {

        RestTemplate restTemplate1 = new RestTemplate();
        HttpHeaders headers = new HttpHeaders();
        headers.set("Authorization", authorizationHeader);
        HttpEntity<Void> requestEntity = new HttpEntity<>(headers);

        try {
            ResponseEntity<Void> response = restTemplate1.exchange(authServiceURL + "/api/v1/validate", HttpMethod.GET, requestEntity, Void.class);
        } catch (HttpClientErrorException e) {
            r.setStatus(HttpStatus.UNAUTHORIZED.value());
            return null;
        }

        RestTemplate restTemplate = new RestTemplate();
        String deleteBookUrl = ioServiceURL + "/api/books" + "/" + bookId;
        return restTemplate.exchange(deleteBookUrl, HttpMethod.DELETE, null, String.class, bookId).getBody();
    }

}
