package com.proiect.business.controller;

import com.proiect.business.model.Event;
import jakarta.servlet.http.HttpServletResponse;
import lombok.AllArgsConstructor;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.*;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

import java.util.List;

@RestController
@AllArgsConstructor
@RequiredArgsConstructor
@RequestMapping("/events")
public class EventController {

    @Value("${docker.ioService}")
    private String ioServiceURL;

    @Value("${docker.authService}")
    private String authServiceURL;

    @GetMapping
    public List<Event> getEvents(@RequestHeader("Authorization") String authorizationHeader, HttpServletResponse r) {

        RestTemplate restTemplate1 = new RestTemplate();
        HttpHeaders headers = new HttpHeaders();
        headers.set("Authorization", authorizationHeader);
        HttpEntity<Void> requestEntity = new HttpEntity<>(headers);

        try {
            ResponseEntity<Void> response = restTemplate1.exchange(authServiceURL + "/api/v1/validate", HttpMethod.GET, requestEntity, Void.class);
        } catch (HttpClientErrorException e) {
            r.setStatus(HttpStatus.UNAUTHORIZED.value());
            return null;
        }

        RestTemplate restTemplate = new RestTemplate();
        ParameterizedTypeReference<List<Event>> responseType =
                new ParameterizedTypeReference<>() {
                };

        return restTemplate.exchange(ioServiceURL + "/api/events", HttpMethod.GET, null, responseType).getBody();
    }

    @PostMapping
    public Event addEvent(@RequestBody Event event, @RequestHeader("Authorization") String authorizationHeader, HttpServletResponse r) {

        RestTemplate restTemplate1 = new RestTemplate();
        HttpHeaders headers1 = new HttpHeaders();
        headers1.set("Authorization", authorizationHeader);
        HttpEntity<Void> requestEntity1 = new HttpEntity<>(headers1);

        try {
            ResponseEntity<Void> response = restTemplate1.exchange(authServiceURL + "/api/v1/validate", HttpMethod.GET, requestEntity1, Void.class);
        } catch (HttpClientErrorException e) {
            r.setStatus(HttpStatus.UNAUTHORIZED.value());
            return null;
        }

        RestTemplate restTemplate = new RestTemplate();
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);

        HttpEntity<Event> requestEntity = new HttpEntity<>(event, headers);

        return restTemplate.postForEntity(ioServiceURL + "/api/events", requestEntity, Event.class).getBody();
    }

    @PutMapping("{eventId}")
    public Event updateEvent(@PathVariable Integer eventId, @RequestBody Event event, @RequestHeader("Authorization") String authorizationHeader, HttpServletResponse r) {

        RestTemplate restTemplate1 = new RestTemplate();
        HttpHeaders headers1 = new HttpHeaders();
        headers1.set("Authorization", authorizationHeader);
        HttpEntity<Void> requestEntity1 = new HttpEntity<>(headers1);

        try {
            ResponseEntity<Void> response = restTemplate1.exchange(authServiceURL + "/api/v1/validate", HttpMethod.GET, requestEntity1, Void.class);
        } catch (HttpClientErrorException e) {
            r.setStatus(HttpStatus.UNAUTHORIZED.value());
            return null;
        }

        RestTemplate restTemplate = new RestTemplate();
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);

        HttpEntity<Event> requestEntity = new HttpEntity<>(event, headers);
        String updateEventUrl = ioServiceURL + "/api/events" + "/" + eventId;
        return restTemplate.exchange(updateEventUrl, HttpMethod.PUT, requestEntity, Event.class, eventId).getBody();
    }

    @DeleteMapping("{eventId}")
    public String deleteEvent(@PathVariable Integer eventId, @RequestHeader("Authorization") String authorizationHeader, HttpServletResponse r) {

        RestTemplate restTemplate1 = new RestTemplate();
        HttpHeaders headers = new HttpHeaders();
        headers.set("Authorization", authorizationHeader);
        HttpEntity<Void> requestEntity = new HttpEntity<>(headers);

        try {
            ResponseEntity<Void> response = restTemplate1.exchange(authServiceURL + "/api/v1/validate", HttpMethod.GET, requestEntity, Void.class);
        } catch (HttpClientErrorException e) {
            r.setStatus(HttpStatus.UNAUTHORIZED.value());
            return null;
        }

        RestTemplate restTemplate = new RestTemplate();
        String deleteEventUrl = ioServiceURL + "/api/events" + "/" + eventId;
        return restTemplate.exchange(deleteEventUrl, HttpMethod.DELETE, null, String.class, eventId).getBody();
    }
}
