package com.proiect.business.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class Book {

    private Integer id;
    private String name;
    private String author;
    private Genres genre;
    private Integer publicationYear;
    private Float price;
}
