package com.proiect.business.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class Event {

    private Integer id;
    private String name;
    private EventType type;
    private String address;
    private Float ticketPrice;
 }
