package com.proiect.business.model;

public enum EventType {
    BOOK_LAUNCH,
    PARTY,
    BALL
}
