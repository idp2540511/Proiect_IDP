package com.proiect.business.model;

public enum Genres {
    FICTION,
    MYSTERY,
    THRILLER,
    ROMANCE,
    FANTASY,
    HORROR
}
