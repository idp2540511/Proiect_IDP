FROM openjdk:17-alpine
WORKDIR /app
COPY target/business-0.0.1-SNAPSHOT.jar /app/business_logic.jar
EXPOSE 8081